package com.rshir.hel.wsdl;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Student{

    String BicToIntCodeResult;
    int age;
    int id;

    public String getBicToIntCodeResult(){
        return BicToIntCodeResult;
    }

    @XmlElement
    public void setBicToIntCodeResult(String BicToIntCodeResult){
        this.BicToIntCodeResult = BicToIntCodeResult;
    }

    public int getAge(){
        return age;
    }

    @XmlElement
    public void setAge(int age){
        this.age = age;
    }

    public int getId(){
        return id;
    }

    @XmlAttribute
    public void setId(int id){
        this.id = id;
    }
}