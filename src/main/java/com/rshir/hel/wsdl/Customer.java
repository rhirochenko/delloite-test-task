package com.rshir.hel.wsdl;

import java.util.*;
import javax.xml.bind.annotation.*;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Customer {
    private List<String> emailAddresses;

    public Customer() {
        emailAddresses = new ArrayList<String>();
    }

    public List<String> getEmailAddresses() {
        return emailAddresses;
    }

    public void setEmailAddresses(List<String> emailAddresses) {
        this.emailAddresses = emailAddresses;
    }
}