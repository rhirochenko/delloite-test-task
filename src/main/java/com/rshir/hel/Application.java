package com.rshir.hel;


import com.rshir.hel.wsdl.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.rshir.hel.wsdl.BicToIntCodeResponse;

import javax.lang.model.element.Element;
import javax.swing.text.Document;
import javax.xml.bind.*;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.*;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class Application {

    public static void main(String args[]) throws Exception {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            String url = "http://www.cbr.ru/CreditInfoWebServ/CreditOrgInfo.asmx?op=GetDatesForF101";
            SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(), url);

            SOAPPart sp = soapResponse.getSOAPPart();
            SOAPEnvelope se = sp.getEnvelope();
            SOAPBody sb = se.getBody();
            SOAPHeader sh = se.getHeader();

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            soapResponse.writeTo(stream);
            String message = new String(stream.toByteArray(), "utf-8");

            // print SOAP Response
            //System.out.print("Response SOAP Message:");
            //soapResponse.writeTo(System.out);

            //JAXBContext jaxbContext = JAXBContext.newInstance(BicToIntCodeResponse.class);
            //Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            //GetDatesForF101Response updateMyDetails = (GetDatesForF101Response) jaxbUnmarshaller.unmarshal(soapResponse.getSOAPBody().extractContentAsDocument());

            File f = new File("/home/rshirochenko/student7.xml");
            FileInputStream fis = new FileInputStream(f);
            GetDatesForF101Response st = JAXB.unmarshal(fis, GetDatesForF101Response.class);

            List<String> res = st.getGetDatesForF101Result();
            System.out.println("First value: " + res.get(0));
            System.out.println("Last value: " + res.get(res.size()-1));

            //System.out.println("Age : " + st.getGetDatesForF101Result());
            System.out.println("Name : " + st.getGetDatesForF101Result());
            //System.out.println("myDetails:" + updateMyDetails.getBicToIntCodeResult());

            soapConnection.close();
    }


        private static SOAPMessage createSOAPRequest() throws Exception {
            MessageFactory messageFactory = MessageFactory.newInstance();
            SOAPMessage soapMessage = messageFactory.createMessage();
            SOAPPart soapPart = soapMessage.getSOAPPart();

            String serverURI = "http://web.cbr.ru/";

            // SOAP Envelope
            SOAPEnvelope envelope = soapPart.getEnvelope();
            //envelope.addNamespaceDeclaration("web", serverURI);

            // SOAP Body
            SOAPBody soapBody = envelope.getBody();
            SOAPElement soapBodyElem = soapBody.addChildElement("GetDatesForF101","","http://web.cbr.ru/");
            SOAPElement soapBodyElem1 = soapBodyElem.addChildElement("CredprgNumber");
            soapBodyElem1.addTextNode("1");

            MimeHeaders headers = soapMessage.getMimeHeaders();
            headers.addHeader("SOAPAction", serverURI  + "GetDatesForF101");

            soapMessage.saveChanges();

        /* Print the request message */
            System.out.print("Request SOAP Message:");
            soapMessage.writeTo(System.out);
            System.out.println();

            return soapMessage;
        }

    /**
     * Method used to print the SOAP Response
     */
    private static void printSOAPResponse(SOAPMessage soapResponse) throws Exception {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        Source sourceContent = soapResponse.getSOAPPart().getContent();
        System.out.print("\nResponse SOAP Message = ");
        StreamResult result = new StreamResult(System.out);
        transformer.transform(sourceContent, result);
    }

}
